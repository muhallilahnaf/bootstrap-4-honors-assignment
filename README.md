# README #

Honors Peer-graded Assignment for the
[Front-End Web UI Frameworks and Tools: Bootstrap 4](https://www.coursera.org/learn/bootstrap-4/home/welcome)
course, first of the four courses of the
[Full-Stack Web Development with React Specialisation](https://www.coursera.org/specializations/full-stack-react)
offered by Coursera.

### Introduction ###
The goal of the project is to have a website at the end which will feature smartphone detailed information, and
reviews too. Some of the features it would support are:

1.  search bar.
2.  table to distinctively show the details or information about the device with pictures.
3.  comparison feature.
4.  homepage with the latest information and devices available, with a carousel.

[UI design and prototype link](https://www.figma.com/file/orQsU8qQaJCJ6DcNrTYaW6/Coursera-Bootstrap-4-Honors-Assignment?node-id=0%3A1)

[Live Demo](http://tannasample.byethost4.com/coursera/full-stack-react/bootstrap4/honors)