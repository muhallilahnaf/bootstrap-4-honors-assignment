'use strict';

module.exports = function (grunt) {
    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Automatically load required Grunt tasks
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    //For grunt-sass latest version
    const sass = require('node-sass');
    require('load-grunt-tasks')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({
        sass: {
            options: {
                implementation: sass,
                sourceMap: true
            },
            dist: {
                files: {
                    'css/style.css': 'css/style.scss'
                }
            }
        },

        clean: {
            build: {
                src: ['dist/']
            }
        },

        copy: {
            html: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '.\\',
                        src: ['*.html'],
                        dest: 'dist'
                    }
                ]
            },
            node_modules: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '.\\',
                        src: [
                            'node_modules/jquery/dist/jquery.slim.min.js',
                            'node_modules/popper.js/dist/popper.min.js',
                            'node_modules/bootstrap/dist/js/bootstrap.min.js',
                            'js/scripts.js'
                        ],
                        dest: 'dist/js',
                        flatten: true,
                        filter: 'isFile'
                    }
                ]
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: '.\\',
                    src: ['img/*.{png,jpg,gif}'],
                    dest: 'dist/'
                }]
            }
        },

        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function (context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0, rebase: false
                                };
                            }
                        }]
                    }
                }
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            // dist configuration is provided by useminPrepare
            dist: {}
        },

        cssmin: {
            dist: {}
        },

        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release: {
                files: [{
                    src: [
                        'dist/css/*.css'
                    ]
                }]
            }
        },

        // Usemin
        // Replaces all assets with their revved version in html and css files.
        // options.assetDirs contains the directories for finding the assets
        // according to their relative paths
        usemin: {
            html: ['dist/compare.html', 'dist/about.html', 'dist/index.html', 'dist/product.html'],
            options: {
                assetsDirs: ['dist', 'dist/css']
            }
        },

        processhtml: {
            dist: {
                options: {
                    commentMarker: "process"
                },
                files: {
                    'dist/index.html': ['dist/index.html'],
                    'dist/compare.html': ['dist/compare.html'],
                    'dist/about.html': ['dist/about.html'],
                    'dist/product.html': ['dist/product.html']
                }
            }
        },

        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true
                },
                files: {          // 'destination': 'source'
                    'dist/index.html': 'dist/index.html',
                    'dist/compare.html': 'dist/compare.html',
                    'dist/about.html': 'dist/about.html',
                    'dist/product.html': 'dist/product.html'
                }
            }
        }
    });

    grunt.registerTask('css', ['sass']);
    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'filerev',
        'usemin',
        'processhtml',
        'htmlmin'
    ]);
};